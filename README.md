# threadtrackr

ThreadTrackr is a web-based database system designed to track samples through different processes,
including the cases where the process results in fractionation of the sample.

## Features

- assign a sample to an experiment
- defined experimental templates
- sample trees, each node assoicates with an event
- event dependency trees
- event parameter sets
- experimental status
- experiment groups
- experiment costing, including cost tiers
- experiment ownership, lab and permissions
- user privileges
- external user ID mapping for AUTH_USER environment variable (integrates with Cosign/EASE, for example)
- Event equipment tracking and evaluation
- sample categories and tags (e.g. genotype, etc)
- scheduling
- event annotations
- comment anywhere, including text, links, pictures

## authors

The design and development of this project is currently undertaken by Jimi Wills and Mark Ditzel, after describing a solution that meets both person's data needs.  Jimi is the coder <jimi.wills@ed.ac.uk>.

