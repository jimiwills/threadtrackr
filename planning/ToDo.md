# To Do

## Users and Labs

Add a couple users and their labs manually.

## Experiments and Groups

Set up the form to make a new experiment.
Also set up handsontable for editing multiple
experiments within a group.

Creating the new experiment should also create the
root sample of the experiment.

# Samples and Events

Set up the code for managing nested sets in samples and events.
Allow splitting a sample, with an associated event.  There should
be an *add sample(s)* action available on each experiment, which
adds a sample to the root sample, expanding the nested set as
necessary.  There should also be the option to specify the order
of the samples ... arrow buttons? drag and drop?

The event types available to add to a sample, or sample group (which
is a front-end grouping) should be filtered based on the dependency
table and what events are in the sample history.  So event propagation
needs to be implemented through the hierarchy.  This also needs to happen
for tags, and categories.

# measurements

I discovered today a new family of tables that needs to exist...
Events can have parameters, but some will also have measurements.
I suppose these could also be stored as parameters.  But actually,
they should be displayed differently, as they are important for the
individual progress of a sample, not just for methodological reference.
A parameter might be 8M urea, which might be very standard.  But 
a measurement might be 50 uL, or 2.54 mg/mL, etc.  And decisions are
made on this basis, so these should be tabulated  in the sample view,
rather than listed below in the parameters... I think.

update: add type field, single character, to parameter type table. p is parameter
m is measurement. front end can distinguish them. back end neednt

Actually, no.  There's a type field in the parameter already, and we can
use that to indicate that the parameter is measured rather than set.

# Layout

I need to made some mock-ups of how the thing will look, with the
different views available.  And also how these things will be searchable.

| Expt Title | Event 1 | Event2 |
|------------|---------|--------|

