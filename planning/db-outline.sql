-- MSSQL//MySQL
-- can have nvarchar(510)//varchar(255)
-- or Ntext//Text (1Gb//65k)
-- int is -2g..+2g in both
-- decminal(total digits, decimal digits) same in both -- good for money?
-- datetime2//datetime -- not sure about format.

CREATE DATABASE threadtrackr;
USE threadtrackr;

CREATE TABLE [Table] (
    TableId int,
    TableName string
);
ALTER TABLE Table ADD CONSTRAINT PK_Table PRIMARY KEY (TableId);

CREATE TABLE [User] (
    Lab int,
    LabId int,
    UserFullName string,
    UserPreferedName string,
    UserEmail string,
    UserId int
);
ALTER TABLE User ADD CONSTRAINT FK_User_LabId FOREIGN KEY (LabId) REFERENCES Lab(LabId);
ALTER TABLE User ADD CONSTRAINT PK_User PRIMARY KEY (UserId);

CREATE TABLE [URLComment] (
    URLName string,
    URL Text,
    URLCommentId int,
    TableId int,
    RowId int
);
ALTER TABLE URLComment ADD CONSTRAINT FK_URLComment_TableId FOREIGN KEY (TableId) REFERENCES Table(TableId);
ALTER TABLE URLComment ADD CONSTRAINT PK_URLComment PRIMARY KEY (URLCommentId);

CREATE TABLE [Lab] (
    LabDefaultExperimentCostTierId int,
    LabDefaultFolder string,
    LabId int,
    LabName string,
    LabLeaderUserId int
);
ALTER TABLE Lab ADD CONSTRAINT FK_Lab_ExperimentCostTierId FOREIGN KEY (ExperimentCostTierId) REFERENCES ExperimentCostTier(ExperimentCostTierId);
ALTER TABLE Lab ADD CONSTRAINT FK_Lab_LabLeaderUserId FOREIGN KEY (LabLeaderUserId) REFERENCES User(UserId);
ALTER TABLE Lab ADD CONSTRAINT PK_Lab PRIMARY KEY (LabId);

CREATE TABLE [Status] (
    UserId int,
    ExperimentId int,
    StatusTime DateTime,
    StatusTypeId int,
    StatusId int
);
ALTER TABLE Status ADD CONSTRAINT FK_Status_ExperimentId FOREIGN KEY (ExperimentId) REFERENCES Experiment(ExperimentId);
ALTER TABLE Status ADD CONSTRAINT FK_Status_StatusTypeId FOREIGN KEY (StatusTypeId) REFERENCES StatusType(StatusTypeId);
ALTER TABLE Status ADD CONSTRAINT PK_Status PRIMARY KEY (StatusId);

CREATE TABLE [StatusType] (
    StatusTypeTitle string,
    StatusTypeId int,
    StatusTypeDescription string
);
ALTER TABLE StatusType ADD CONSTRAINT PK_StatusType PRIMARY KEY (StatusTypeId);

CREATE TABLE [EventDep] (
    EventDepId int,
    EventDep int,
    EventTypeId int,
    EventDepInvert Boolean
);
ALTER TABLE EventDep ADD CONSTRAINT FK_EventDep_EventTypeId FOREIGN KEY (EventTypeId) REFERENCES EventType(EventTypeId);
ALTER TABLE EventDep ADD CONSTRAINT FK_EventDep_EventDep FOREIGN KEY (EventDep) REFERENCES EventType(EventTypeId);
ALTER TABLE EventDep ADD CONSTRAINT PK_EventDep PRIMARY KEY (EventDepId);

CREATE TABLE [ExperimentCost] (
    ExperimentCostTierId int,
    ExperimentCostId int,
    ExperimentTypeId int,
    ExperimentCostPerSample int
);
ALTER TABLE ExperimentCost ADD CONSTRAINT FK_ExperimentCost_ExperimentTypeId FOREIGN KEY (ExperimentTypeId) REFERENCES ExperimentType(ExperimentTypeId);
ALTER TABLE ExperimentCost ADD CONSTRAINT PK_ExperimentCost PRIMARY KEY (ExperimentCostId);

CREATE TABLE [UserPrivileges] (
    PrivilegeId int,
    UserId int
);
ALTER TABLE UserPrivileges ADD CONSTRAINT FK_UserPrivileges_UserId FOREIGN KEY (UserId) REFERENCES User(UserId);
ALTER TABLE UserPrivileges ADD CONSTRAINT FK_UserPrivileges_PrivilegeId FOREIGN KEY (PrivilegeId) REFERENCES Privilege(PrivilegeId);
ALTER TABLE UserPrivileges ADD CONSTRAINT PK_UserPrivileges PRIMARY KEY (PrivilegeId,UserId);

CREATE TABLE [Parameter] (
    ParameterDescription string,
    ParameterType ENUM(number,string) = int,
    ParameterId int,
    ParameterName string
);
ALTER TABLE Parameter ADD CONSTRAINT PK_Parameter PRIMARY KEY (ParameterId);

CREATE TABLE [Schedule] (
    ScheduleDateTime datetime,
    EventId int,
    ScheduleId int
);
ALTER TABLE Schedule ADD CONSTRAINT FK_Schedule_EventId FOREIGN KEY (EventId) REFERENCES Event(EventId);
ALTER TABLE Schedule ADD CONSTRAINT PK_Schedule PRIMARY KEY (ScheduleId);

CREATE TABLE [TextComment] (
    Comment Text,
    RowId int,
    TableId int,
    TextCommentId int
);
ALTER TABLE TextComment ADD CONSTRAINT FK_TextComment_TableId FOREIGN KEY (TableId) REFERENCES Table(TableId);
ALTER TABLE TextComment ADD CONSTRAINT PK_TextComment PRIMARY KEY (TextCommentId);

CREATE TABLE [Evaluation] (
    EvaluationId int,
    EvaluationTitle string,
    EquipmentId int,
    EventId int
);
ALTER TABLE Evaluation ADD CONSTRAINT FK_Evaluation_EventId FOREIGN KEY (EventId) REFERENCES Event(EventId);
ALTER TABLE Evaluation ADD CONSTRAINT FK_Evaluation_EquipmentId FOREIGN KEY (EquipmentId) REFERENCES Equipment(EquipmentId);
ALTER TABLE Evaluation ADD CONSTRAINT PK_Evaluation PRIMARY KEY (EvaluationId);

CREATE TABLE [ExperimentGroup] (
    Mtr int = NULL,
    Ftr int = NULL,
    ExternalId string,
    ExperimentGroupReason string,
    ExternalSize int,
    ExperimentGroupId int,
    Date date
);
ALTER TABLE ExperimentGroup ADD CONSTRAINT PK_ExperimentGroup PRIMARY KEY (ExperimentGroupId);

CREATE TABLE [Category] (
    CategoryTitle text,
    CategoryTime DateTime,
    CategorySetId int,
    CategoryId int
);
ALTER TABLE Category ADD CONSTRAINT PK_Category PRIMARY KEY (CategoryId);

CREATE TABLE [Equipment] (
    EquipmentId int,
    EquipmentName string,
    EventTypeId int
);
ALTER TABLE Equipment ADD CONSTRAINT FK_Equipment_EventTypeId FOREIGN KEY (EventTypeId) REFERENCES EventType(EventTypeId);
ALTER TABLE Equipment ADD CONSTRAINT PK_Equipment PRIMARY KEY (EquipmentId);

CREATE TABLE [EventType] (
    EventTypeTitle string,
    EventTypeId int,
    EventTypeDescription string,
    ParameterSetId int
);
ALTER TABLE EventType ADD CONSTRAINT PK_EventType PRIMARY KEY (EventTypeId);

CREATE TABLE [BlobComment] (
    BlobName string,
    BlobCommentId int,
    BlobMimeType string,
    RowId int,
    TableId int,
    Blob Blob
);
ALTER TABLE BlobComment ADD CONSTRAINT FK_BlobComment_TableId FOREIGN KEY (TableId) REFERENCES Table(TableId);
ALTER TABLE BlobComment ADD CONSTRAINT PK_BlobComment PRIMARY KEY (BlobCommentId);

CREATE TABLE [Experiment] (
    ExperimentCostPounds int,
    ExperimentTitle string,
    Description string,
    ExperimentGroup int = NULL,
    ExperimentCostId int,
    ExperimentSampleCount int,
    ExperimentPebbleCode string,
    ExperimentId int,
    FolderId int,
    ExperimentTypeId int
);
ALTER TABLE Experiment ADD CONSTRAINT FK_Experiment_ExperimentGroupId FOREIGN KEY (ExperimentGroupId) REFERENCES ExperimentGroup(ExperimentGroupId);
ALTER TABLE Experiment ADD CONSTRAINT FK_Experiment_ExperimentCostId FOREIGN KEY (ExperimentCostId) REFERENCES ExperimentCost(ExperimentCostId);
ALTER TABLE Experiment ADD CONSTRAINT FK_Experiment_ExperimentTypeId FOREIGN KEY (ExperimentTypeId) REFERENCES ExperimentType(ExperimentTypeId);
ALTER TABLE Experiment ADD CONSTRAINT FK_Experiment_FolderId FOREIGN KEY (FolderId) REFERENCES Folder(FolderId);
ALTER TABLE Experiment ADD CONSTRAINT PK_Experiment PRIMARY KEY (ExperimentId);

CREATE TABLE [ExperimentCostTier] (
    ExperimentCostTierTitle string,
    ExperimentCostTierDescription string,
    ExperimentCostTierId int
);
ALTER TABLE ExperimentCostTier ADD CONSTRAINT PK_ExperimentCostTier PRIMARY KEY (ExperimentCostTierId);

CREATE TABLE [CategorySet] (
    CategorySetName string,
    CategorySetId int,
    CategorySetDescription string
);
ALTER TABLE CategorySet ADD CONSTRAINT PK_CategorySet PRIMARY KEY (CategorySetId);

CREATE TABLE [ExperimentOwners] (
    ExperimentId int,
    UserId int
);
ALTER TABLE ExperimentOwners ADD CONSTRAINT FK_ExperimentOwners_UserId FOREIGN KEY (UserId) REFERENCES User(UserId);
ALTER TABLE ExperimentOwners ADD CONSTRAINT FK_ExperimentOwners_ExperimentId FOREIGN KEY (ExperimentId) REFERENCES Experiment(ExperimentId);
ALTER TABLE ExperimentOwners ADD CONSTRAINT PK_ExperimentOwners PRIMARY KEY (UserId,ExperimentId);

CREATE TABLE [UUN] (
    UserId int,
    UUN string (PK)
);
ALTER TABLE UUN ADD CONSTRAINT FK_UUN_UserId FOREIGN KEY (UserId) REFERENCES User(UserId);
ALTER TABLE UUN ADD CONSTRAINT PK_UUN PRIMARY KEY (UUN);

CREATE TABLE [TagWord] (
    lft int,
    TagWord string,
    TagWordId int,
    rgt int,
    TagWordDescription string
);
ALTER TABLE TagWord ADD CONSTRAINT PK_TagWord PRIMARY KEY (TagWordId);

CREATE TABLE [Folder] (
    Folder string,
    FolderId int
);
ALTER TABLE Folder ADD CONSTRAINT PK_Folder PRIMARY KEY (FolderId);

CREATE TABLE [Event] (
    rgt int,
    SampleId int,
    EventId int,
    EventTypeId int,
    lft int
);
ALTER TABLE Event ADD CONSTRAINT FK_Event_SampleId FOREIGN KEY (SampleId) REFERENCES Sample(SampleId);
ALTER TABLE Event ADD CONSTRAINT FK_Event_EventTypeId FOREIGN KEY (EventTypeId) REFERENCES EventType(EventTypeId);
ALTER TABLE Event ADD CONSTRAINT PK_Event PRIMARY KEY (EventId);

CREATE TABLE [AnnotationType] (
    AnnotationTypeTitle string,
    AnnotationTypeDescription string,
    AnnotationTypeId int,
    EventTypeId int
);
ALTER TABLE AnnotationType ADD CONSTRAINT FK_AnnotationType_EventTypeId FOREIGN KEY (EventTypeId) REFERENCES EventType(EventTypeId);
ALTER TABLE AnnotationType ADD CONSTRAINT PK_AnnotationType PRIMARY KEY (AnnotationTypeId);

CREATE TABLE [Annotation] (
    UserId int,
    AnnotationTypeId int,
    EventId int,
    AnnotationTime DateTime,
    AnnotationId int
);
ALTER TABLE Annotation ADD CONSTRAINT FK_Annotation_EventId FOREIGN KEY (EventId) REFERENCES Event(EventId);
ALTER TABLE Annotation ADD CONSTRAINT FK_Annotation_AnnotationTypeId FOREIGN KEY (AnnotationTypeId) REFERENCES AnnotationType(AnnotationTypeId);
ALTER TABLE Annotation ADD CONSTRAINT FK_Annotation_UserId FOREIGN KEY (UserId) REFERENCES User(UserId);
ALTER TABLE Annotation ADD CONSTRAINT PK_Annotation PRIMARY KEY (AnnotationId);

CREATE TABLE [Sample] (
    SampleComment string,
    rgt int,
    SampleId int,
    SampleName string,
    lft int,
    ExperimentId int
);
ALTER TABLE Sample ADD CONSTRAINT FK_Sample_ExperimentId FOREIGN KEY (ExperimentId) REFERENCES Experiment(ExperimentId);
ALTER TABLE Sample ADD CONSTRAINT PK_Sample PRIMARY KEY (SampleId);

CREATE TABLE [SampleCategory] (
    SampleCategoryId int,
    SampleId int,
    CategoryId int,
    CategoryTime DateTime
);
ALTER TABLE SampleCategory ADD CONSTRAINT FK_SampleCategory_CategorySetId FOREIGN KEY (CategorySetId) REFERENCES CategorySet(CategorySetId);
ALTER TABLE SampleCategory ADD CONSTRAINT FK_SampleCategory_SampleId FOREIGN KEY (SampleId) REFERENCES Sample(SampleId);
ALTER TABLE SampleCategory ADD CONSTRAINT FK_SampleCategory_CategoryId FOREIGN KEY (CategoryId) REFERENCES Category(CategoryId);
ALTER TABLE SampleCategory ADD CONSTRAINT PK_SampleCategory PRIMARY KEY (SampleCategoryId);

CREATE TABLE [EventTypeParameters] (
    EventTypeId int,
    ParameterId int
);
ALTER TABLE EventTypeParameters ADD CONSTRAINT FK_EventTypeParameters_EventTypeId FOREIGN KEY (EventTypeId) REFERENCES EventType(EventTypeId);
ALTER TABLE EventTypeParameters ADD CONSTRAINT FK_EventTypeParameters_ParameterId FOREIGN KEY (ParameterId) REFERENCES Parameter(ParameterId);
ALTER TABLE EventTypeParameters ADD CONSTRAINT PK_EventTypeParameters PRIMARY KEY (EventTypeId,ParameterId);

CREATE TABLE [ParameterStringValue] (
    ParameterStringValue string,
    ParameterId int,
    EventId int
);
ALTER TABLE ParameterStringValue ADD CONSTRAINT FK_ParameterStringValue_EventId FOREIGN KEY (EventId) REFERENCES Event(EventId);
ALTER TABLE ParameterStringValue ADD CONSTRAINT FK_ParameterStringValue_ParameterId FOREIGN KEY (ParameterId) REFERENCES Parameter(ParameterId);
ALTER TABLE ParameterStringValue ADD CONSTRAINT PK_ParameterStringValue PRIMARY KEY (ParameterId,EventId);

CREATE TABLE [Privilege] (
    PrivilegeId int,
    PrivilegeTitle string,
    PrivilegeDescription string
);
ALTER TABLE Privilege ADD CONSTRAINT PK_Privilege PRIMARY KEY (PrivilegeId);

CREATE TABLE [ExperimentType] (
    ExperimentTypeId int,
    ExperimentTypeTitle string,
    ExperimentTypeDescription string
);
ALTER TABLE ExperimentType ADD CONSTRAINT PK_ExperimentType PRIMARY KEY (ExperimentTypeId);

CREATE TABLE [ParameterNumberValue] (
    ParameterId int,
    ParameterNumberValue int,
    EventId int
);
ALTER TABLE ParameterNumberValue ADD CONSTRAINT FK_ParameterNumberValue_ParameterId FOREIGN KEY (ParameterId) REFERENCES Parameter(ParameterId);
ALTER TABLE ParameterNumberValue ADD CONSTRAINT FK_ParameterNumberValue_EventId FOREIGN KEY (EventId) REFERENCES Event(EventId);
ALTER TABLE ParameterNumberValue ADD CONSTRAINT PK_ParameterNumberValue PRIMARY KEY (ParameterId,EventId);

CREATE TABLE [Tag] (
    TagTime DateTime,
    SampleId int,
    TagId int,
    TagWordId int
);
ALTER TABLE Tag ADD CONSTRAINT FK_Tag_SampleId FOREIGN KEY (SampleId) REFERENCES Sample(SampleId);
ALTER TABLE Tag ADD CONSTRAINT FK_Tag_TagWordId FOREIGN KEY (TagWordId) REFERENCES TagWord(TagWordId);
ALTER TABLE Tag ADD CONSTRAINT PK_Tag PRIMARY KEY (TagId);

