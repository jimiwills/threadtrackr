# ui notes

I always start from the UI (once I've designed data storage)
because this should be the primary point of design since I 
want to enable interaction with the data in the desired ways
and not constrain mode of interaction by designing the 
interfaces to data first.

So here it is...

## data point

a data point, i.e. a particular value from a particular column
of a particular record, should be representable using any html
container element, e.g. div, td, etc.  This being the case, we
can use w3css, or whatever to style any elements as necessary.

### definition

the table, column and record of a data point can be defined by
data-* attributes in html5.  These can be use in querying the server
for:

1. Data refresh
2. User privileges on the field
3. Data editing options
4. Update
5. Any value that differs from the displayed value

### data editing options

on-click (or possible from tabbing off another adjacent field?)
a data-point shoulldl become editable (if privileges allow.)
But the way in which the data can be edited will be determined
by a query to the database which will advise the front end on the
mode of data entry (free-text, select, combo, date, etc) and any
options (list of options for select/combo)

## queries

Data available to the server-side should be:

+ what type of query
   - get-value
   - get-privilege
   - set-value
   - get-input-mode
   - get-options
+ data-point identifiers
   - data-table
   - data-column
   - data-id
+ user information
   - primary user ID (via cosign) form database
   - user role
   - user-data relationship (owner/not-owner)
   - role privileges on data with given ownership

The server-side then needs to:

- decide whether user is allowed to perform query
  (e.g. get privilege is always allowed, set-value depends
  on role-ownership status)
- perform the query
- return the relevant data to the front-end

## ui updates

when the server returns the data, the front end needs to either

1. show an error message
2. update the ui with the relevant data

## layers

1. ui
2. fe/be comms
3. perms checks
4. query functions
5. queries
6. be/fe comms

In terms of MVC, the query functions and query are the model, the
fe/be comms could be seen as controller and be/fe as the view.  There's
then going to be a front controller on the back-end, and kind of the
whole thing mirrored on the front-end (but without model, and with any
data-access functions accessing be-comms rather than data)


## Plan for implementation

I could either build units that do specific jobs, which
would reqiure all of them to be planned in advance, or
I could start from the begining - user arrives at page
for the first time - and implement each thing that needs
to happen to make it work... which would require a 
continuous stream of consciousness (pretty much.)

### The planned way

Could start with inserts of different types of data, then
queries and updates.  Would need to catalogue the main
types of access up-front.

+ insert/update user
   - is the first thing a user would experience
   - would also involve group and role
+ new experiment
   - obviously involves looking up sample too
   - need not have any events up-front
+ edit experiment
   - add an event
   - add samples on an event
   - remove event, etc
+ add tags, statuses, comments, etc

There's a whole bunch more

### The organic way

I like this way because in any case we can edit the data
in phpMyAdmin to start with, so only the more pressing or
exciting items need to be implemented for the system to be
usable.  Notably, the planned version would need planned
out the management of tags, event types, etc, at the outset,
whereas in the organic approach these can be edited manually
until the typical working method is established.

So, what's first with the organic approach?

Skip user admin UI.  We don't need it to get the database
usable/testable, and Mark doesn't need it at all.

Skip pre-designed experiments to start with, as I'm still
not sure whether to copy them from templates or build
routines to set them up.

So we need to be able to:

+ insert/update/(remove?) and obviously select an experiment
  and associated root sample
+ determine allowed events at a particular stage and add one
  this requires a query to look up previous events on a sample
+ choose which samples an event happens to
+ set how many samples arise from a sample during an event
+ edit other sample details

A lot of this can be done modally (ie. with dialogs) but
cloud also be done in tabular format.  I want to try to 
avoid doing any tabular work until later, preferring 
pma for that... need to give Mark a tutorial!

The non-tabular part basically all fits into the experiment
view I have in mind.  So the starting point should be that.
A list of experiments to choose from, an add experiment
button, and the main view for an experiment.

### Experiment view (reminder)

Samples down the side, events along the top, details in
the middle, timeline along the top/bottom.

### The real starting point for the organic approach

add some data to the database... especially samples, events,
allowed/disallowed event orders, etc.

So actually, the 

