# Request Type and Action Handlers

I would like for my action handler modules to each provide the M, V and C for their domain. 

I would also like for them to blindly handle AJAX or "inline" requests.  This means the handover for each needs to be the same, and the return has to be agnostic too.

This can easily be achieved by the dispatcher detecting the style of request and either placing the action in the context of a full page or calling it alone and directly printing the response.  Of course, the response for this type of request will be HTML in both cases, and not XML. 

# request structure

There are several items that need to be defined...

1) view - what type of view, and in some cases, what types for several views on the one page; each view type may have associated parameters.  Probably, this kind of compound view should still be seen as a single view.  Subcomponenets of a compound view can get updated independently via AJAX.

2) data - which data are getting displayed in which view? (model)

3) action - some data getting updated (controller)

The thing that does the interprettation of the URL to figure out the action to be performed, and what MVC parts that requires is the front-controller.  This is the front-end of the server-side, but there's still the script on the page that does a bunch of stuff too, like populating a HOT.

# mvc?

Of course we don't have to adhere to mvc.  Or we don't have to implement mvc on the server (it could be on the client.)

The things I like about having the majority of functionality on the client is that it:

1. Puts some more load on the client instead of the server
2. Simplifies server code and its responsibility
3. Allows simple modularisation of a page
4. Far simpler reorganisation of page layout
5. Most dev will be JS; backend can be anything, because it won't be a lot
6. Front-to-back-end communication protocol can be really generic
7. Most error handling is in the browser, so the browser code can decide where to put the messages
8. Conceivable to have client-side cache of database (or parts thereof) for offline analysis 

The things I don't like about it:

1. not all browsers will be capable - safari? (people can be encouraged to use a capable browser!)
2. Error logging is dependent on script being able to contact the server to log it
3. SQL queries should be back-end if back-end is handling security (which it should) - so how do we get use front-end to manage queries?
