# threadtrackr database actions at sql level

## Tables with standard interfaces

- `Folder`
- `ExperimentCostTier`
- `Privilege`
- `ExperimentGroup`
- `ExperimentType`

## `User`

Most things about `User` and `UUN` are pretty straightforward.

### select by `UUN`

    SELECT * FROM `User` JOIN UUN ON `User`.`UserId` = `UUN`.`UserId` WHERE `UUN`.`UUN` = %s

## `Lab`

`Lab` references `ExperimentCostTier`, `User`, `Folder`

## `UserPrivileges`

`UserPrivileges` references `User` and `Privilege`

## `ExperimentCost`

`ExperimentCost` references `ExperimentType`, `ExperimentCostTier`

## `Experiment`

`Experiment` references `ExperimentGroup`, `ExperimentCost`, `ExperimentType`

## `ExperimentOwners`



# to be honest...

I think most items will be mapped 1:1 with their objects in PHP.
The only "complicated" stuff required in SQL is where JOINs are needed
and the handling of ***nested sets***.

# Drawings

I need to draw out how ***everything*** is going to look.