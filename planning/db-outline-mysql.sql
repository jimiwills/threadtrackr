-- MSSQL//MySQL
-- can have nvarchar(510)//VARCHAR(255)
-- or Ntext//Text (1Gb//65k)
-- INT UNSIGNED is -2g..+2g in both
-- decminal(total digits, decimal digits) same in both -- good for money?
-- float // double
-- DATETIME2//DATETIME -- not sure about format.
-- [name] // `name`
-- IDENTITY(1,1) // AUTO_INCREMENT
-- but You can't alter the existing columns for identity...
--    identity needs to be created during table creation
--    while auto_increment requires the column is a key
--    so we do it with alter in this case, since we're setting
--    keys with alter.

DROP DATABASE IF EXISTS threadtrackr;

CREATE DATABASE threadtrackr
    DEFAULT CHARACTER SET utf16
    DEFAULT COLLATE utf16_unicode_ci
;
USE threadtrackr;


-- ==============================================================================--
-- USERS AND LABS

-- `User` table

CREATE TABLE `User` (
    `UserId` INT UNSIGNED,
    `UserFullName` VARCHAR(255),
    `UserPreferedName` VARCHAR(255),
    `UserEmail` VARCHAR(255),
    `Lab` INT UNSIGNED,
    `LabId` INT UNSIGNED
);
ALTER TABLE `User` ADD CONSTRAINT `PK_User` PRIMARY KEY (UserId);
ALTER TABLE `User` MODIFY COLUMN `UserId` INT UNSIGNED auto_increment;
-- FK defined below (Interdependency)

-- --------------------------------------------------------------------------------
-- Lab

-- lab folder
CREATE TABLE `Folder` (
    `FolderId` INT UNSIGNED,
    `Folder` VARCHAR(255)
);
ALTER TABLE `Folder` ADD CONSTRAINT `PK_Folder` PRIMARY KEY (FolderId);
ALTER TABLE `Folder` MODIFY COLUMN `FolderId` INT UNSIGNED auto_increment;

-- cost tiers
CREATE TABLE `ExperimentCostTier` (
    `ExperimentCostTierId` INT UNSIGNED,
    `ExperimentCostTierTitle` VARCHAR(255),
    `ExperimentCostTierDescription` TEXT
);
ALTER TABLE `ExperimentCostTier` ADD CONSTRAINT `PK_ExperimentCostTier` PRIMARY KEY (ExperimentCostTierId);
ALTER TABLE `ExperimentCostTier` MODIFY COLUMN `ExperimentCostTierId` INT UNSIGNED auto_increment;


-- `Lab` references `ExperimentCostTier`, `User`, `Folder`
CREATE TABLE `Lab` (
    `LabId` INT UNSIGNED,
    `LabName` VARCHAR(255),
    `LabLeaderUserId` INT UNSIGNED,
    `LabDefaultExperimentCostTierId` INT UNSIGNED,
    `LabDefaultFolder` INT UNSIGNED
);
ALTER TABLE `Lab` ADD CONSTRAINT `FK_Lab_ExperimentCostTierId` FOREIGN KEY (LabDefaultExperimentCostTierId) REFERENCES `ExperimentCostTier`(ExperimentCostTierId);
ALTER TABLE `Lab` ADD CONSTRAINT `FK_Lab_LabLeaderUserId` FOREIGN KEY (LabLeaderUserId) REFERENCES `User`(UserId);
ALTER TABLE `Lab` ADD CONSTRAINT `FK_Lab_LabDefaultFolder` FOREIGN KEY (LabDefaultFolder) REFERENCES `Folder`(FolderId);
ALTER TABLE `Lab` ADD CONSTRAINT `PK_Lab` PRIMARY KEY (LabId);
ALTER TABLE `Lab` MODIFY COLUMN `LabId` INT UNSIGNED auto_increment;

-- Now that `Lab` is created, we can reference it from `User`
ALTER TABLE `User` ADD CONSTRAINT `FK_User_LabId` FOREIGN KEY (LabId) REFERENCES `Lab`(LabId);

-- --------------------------------------------------------------------------------
-- `UUN` references `User`

CREATE TABLE `UUN` (
    `UUN` VARCHAR(100),
    `UserId` INT UNSIGNED
);
ALTER TABLE `UUN` ADD CONSTRAINT `PK_UUN` PRIMARY KEY (UUN);
ALTER TABLE `UUN` ADD CONSTRAINT `FK_UUN_UserId` FOREIGN KEY (UserId) REFERENCES `User`(UserId);
ALTER TABLE `UUN` MODIFY COLUMN `UUN` INT UNSIGNED auto_increment;

-- --------------------------------------------------------------------------------
-- User privileges
CREATE TABLE `Privilege` (
    `PrivilegeId` INT UNSIGNED,
    `PrivilegeTitle` VARCHAR(255),
    `PrivilegeDescription` TEXT
);
ALTER TABLE `Privilege` ADD CONSTRAINT `PK_Privilege` PRIMARY KEY (PrivilegeId);
ALTER TABLE `Privilege` MODIFY COLUMN `PrivilegeId` INT UNSIGNED auto_increment;

-- `UserPrivileges` references `User` and `Privilege`

CREATE TABLE `UserPrivileges` (
    `UserId` INT UNSIGNED,
    `PrivilegeId` INT UNSIGNED
);
ALTER TABLE `UserPrivileges` ADD CONSTRAINT `PK_UserPrivileges` PRIMARY KEY (PrivilegeId,UserId);
ALTER TABLE `UserPrivileges` ADD CONSTRAINT `FK_UserPrivileges_UserId` FOREIGN KEY (UserId) REFERENCES `User`(UserId);
ALTER TABLE `UserPrivileges` ADD CONSTRAINT `FK_UserPrivileges_PrivilegeId` FOREIGN KEY (PrivilegeId) REFERENCES `Privilege`(PrivilegeId);


-- ==============================================================================--
-- EXPERIMENTS

CREATE TABLE `ExperimentGroup` (
    `ExperimentGroupId` INT UNSIGNED,
    `ExperimentGroupReason` VARCHAR(255),
    `ExperimentGroupDatetime` DATETIME,
    `ExternalId` VARCHAR(255),
    `ExternalSize` INT UNSIGNED,
    `Mtr` INT UNSIGNED,
    `Ftr` INT UNSIGNED
);
ALTER TABLE `ExperimentGroup` ADD CONSTRAINT `PK_ExperimentGroup` PRIMARY KEY (ExperimentGroupId);
ALTER TABLE `ExperimentGroup` MODIFY COLUMN `ExperimentGroupId` INT UNSIGNED auto_increment;


CREATE TABLE `ExperimentType` (
    `ExperimentTypeId` INT UNSIGNED,
    `ExperimentTypeTitle` VARCHAR(255),
    `ExperimentTypeDescription` TEXT
);
ALTER TABLE `ExperimentType` ADD CONSTRAINT `PK_ExperimentType` PRIMARY KEY (ExperimentTypeId);
ALTER TABLE `ExperimentType` MODIFY COLUMN `ExperimentTypeId` INT UNSIGNED auto_increment;

-- `ExperimentCost` references `ExperimentType`, `ExperimentCostTier`

CREATE TABLE `ExperimentCost` (
    `ExperimentCostId` INT UNSIGNED,
    `ExperimentCostTierId` INT UNSIGNED,
    `ExperimentTypeId` INT UNSIGNED,
    `ExperimentCostPerSample` DECIMAL(10,3)
);
ALTER TABLE `ExperimentCost` ADD CONSTRAINT `PK_ExperimentCost` PRIMARY KEY (ExperimentCostId);
ALTER TABLE `ExperimentCost` ADD CONSTRAINT `FK_ExperimentCost_ExperimentTypeId` FOREIGN KEY (ExperimentTypeId) REFERENCES `ExperimentType`(ExperimentTypeId);
ALTER TABLE `ExperimentCost` ADD CONSTRAINT `FK_ExperimentCost_ExperimentCostTierId` FOREIGN KEY (ExperimentCostTierId) REFERENCES `ExperimentCostTier`(ExperimentCostTierId);
ALTER TABLE `ExperimentCost` MODIFY COLUMN `ExperimentCostId` INT UNSIGNED auto_increment;

-- `Experiment` references `ExperimentGroup`, `ExperimentCost`, `ExperimentType`

CREATE TABLE `Experiment` (
    `ExperimentId` INT UNSIGNED,
    `ExperimentTypeId` INT UNSIGNED,
    `ExperimentTitle` VARCHAR(255),
    `Description` TEXT,
    `ExperimentPebbleCode` VARCHAR(255),
    `ExperimentGroupId` INT UNSIGNED,
    `FolderId` INT UNSIGNED,
    `ExperimentCostPounds` INT UNSIGNED,
    `ExperimentCostId` INT UNSIGNED,
    `ExperimentSampleCount` INT UNSIGNED
);
ALTER TABLE `Experiment` ADD CONSTRAINT `PK_Experiment` PRIMARY KEY (ExperimentId);
ALTER TABLE `Experiment` ADD CONSTRAINT `FK_Experiment_ExperimentGroupId` FOREIGN KEY (ExperimentGroupId) REFERENCES `ExperimentGroup`(ExperimentGroupId);
ALTER TABLE `Experiment` ADD CONSTRAINT `FK_Experiment_ExperimentCostId` FOREIGN KEY (ExperimentCostId) REFERENCES `ExperimentCost`(ExperimentCostId);
ALTER TABLE `Experiment` ADD CONSTRAINT `FK_Experiment_ExperimentTypeId` FOREIGN KEY (ExperimentTypeId) REFERENCES `ExperimentType`(ExperimentTypeId);
ALTER TABLE `Experiment` ADD CONSTRAINT `FK_Experiment_FolderId` FOREIGN KEY (FolderId) REFERENCES `Folder`(FolderId);
ALTER TABLE `Experiment` MODIFY COLUMN `ExperimentId` INT UNSIGNED auto_increment;

-- `ExperimentOwners` references `User` and `Experiment`
CREATE TABLE `ExperimentOwners` (
    `ExperimentId` INT UNSIGNED,
    `UserId` INT UNSIGNED
);
ALTER TABLE `ExperimentOwners` ADD CONSTRAINT `PK_ExperimentOwners` PRIMARY KEY (UserId,ExperimentId);
ALTER TABLE `ExperimentOwners` ADD CONSTRAINT `FK_ExperimentOwners_UserId` FOREIGN KEY (UserId) REFERENCES `User`(UserId);
ALTER TABLE `ExperimentOwners` ADD CONSTRAINT `FK_ExperimentOwners_ExperimentId` FOREIGN KEY (ExperimentId) REFERENCES `Experiment`(ExperimentId);


-- --------------------------------------------------------------------------------
-- Experimental Status

CREATE TABLE `StatusType` (
    `StatusTypeId` INT UNSIGNED,
    `StatusTypeTitle` VARCHAR(255),
    `StatusTypeDescription` TEXT
);
ALTER TABLE `StatusType` ADD CONSTRAINT `PK_StatusType` PRIMARY KEY (StatusTypeId);
ALTER TABLE `StatusType` MODIFY COLUMN `StatusTypeId` INT UNSIGNED auto_increment;

-- `Status` References `Experiment` and `StatusType`
CREATE TABLE `Status` (
    `StatusId` INT UNSIGNED,
    `StatusTypeId` INT UNSIGNED,
    `ExperimentId` INT UNSIGNED,
    `UserId` INT UNSIGNED,
    `StatusTime` DATETIME
);
ALTER TABLE `Status` ADD CONSTRAINT `PK_Status` PRIMARY KEY (StatusId);
ALTER TABLE `Status` ADD CONSTRAINT `FK_Status_ExperimentId` FOREIGN KEY (ExperimentId) REFERENCES `Experiment`(ExperimentId);
ALTER TABLE `Status` ADD CONSTRAINT `FK_Status_StatusTypeId` FOREIGN KEY (StatusTypeId) REFERENCES `StatusType`(StatusTypeId);
ALTER TABLE `Status` MODIFY COLUMN `StatusId` INT UNSIGNED auto_increment;

-- ==============================================================================--
-- SAMPLES

-- `Sample` references `Experiment`
CREATE TABLE `Sample` (
    `SampleId` INT UNSIGNED,
    `ExperimentId` INT UNSIGNED,
    `SampleName` VARCHAR(255),
    `SampleComment` TEXT,
    `lft` INT UNSIGNED,
    `rgt` INT UNSIGNED
);
ALTER TABLE `Sample` ADD CONSTRAINT `PK_Sample` PRIMARY KEY (SampleId);
ALTER TABLE `Sample` ADD CONSTRAINT `FK_Sample_ExperimentId` FOREIGN KEY (ExperimentId) REFERENCES `Experiment`(ExperimentId);
ALTER TABLE `Sample` MODIFY COLUMN `SampleId` INT UNSIGNED auto_increment;


-- --------------------------------------------------------------------------------
-- Sample Categories...

CREATE TABLE `CategorySet` (
    `CategorySetId` INT UNSIGNED, 
    `CategorySetName` VARCHAR(255),
    `CategorySetDescription` TEXT
);
ALTER TABLE `CategorySet` ADD CONSTRAINT `PK_CategorySet` PRIMARY KEY (CategorySetId);
ALTER TABLE `CategorySet` MODIFY COLUMN `CategorySetId` INT UNSIGNED auto_increment;

-- `Category` references `CategorySet`
CREATE TABLE `Category` (
    `CategoryId` INT UNSIGNED,
    `CategorySetId` INT UNSIGNED,
    `CategoryTitle` VARCHAR(255),
    `CategoryTime` DATETIME
);
ALTER TABLE `Category` ADD CONSTRAINT `PK_Category` PRIMARY KEY (CategoryId);
ALTER TABLE `Category` ADD CONSTRAINT `FK_Category_CategorySetId` FOREIGN KEY (CategorySetId) REFERENCES `CategorySet`(CategorySetId);
ALTER TABLE `Category` MODIFY COLUMN `CategoryId` INT UNSIGNED auto_increment;

-- `SampleCategory` references `CategorySet`, `Sample`, `Category`
CREATE TABLE `SampleCategory` (
    `SampleId` INT UNSIGNED,
    `CategorySetId` INT UNSIGNED,
    `CategoryId` INT UNSIGNED,
    `CategoryTime` DATETIME
);
ALTER TABLE `SampleCategory` ADD CONSTRAINT `PK_SampleCategory` PRIMARY KEY (SampleId,CategorySetId);
ALTER TABLE `SampleCategory` ADD CONSTRAINT `FK_SampleCategory_SampleId` FOREIGN KEY (SampleId) REFERENCES `Sample`(SampleId);
ALTER TABLE `SampleCategory` ADD CONSTRAINT `FK_SampleCategory_CategorySetId` FOREIGN KEY (CategorySetId) REFERENCES `CategorySet`(CategorySetId);
ALTER TABLE `SampleCategory` ADD CONSTRAINT `FK_SampleCategory_CategoryId` FOREIGN KEY (CategoryId) REFERENCES `Category`(CategoryId);

-- --------------------------------------------------------------------------------
-- Sample Tags 

-- tag-words
CREATE TABLE `TagWord` (
    `TagWordId` INT UNSIGNED,
    `TagWord` VARCHAR(255),
    `TagWordDescription` TEXT,
    `lft` INT UNSIGNED,
    `rgt` INT UNSIGNED
);
ALTER TABLE `TagWord` ADD CONSTRAINT `PK_TagWord` PRIMARY KEY (TagWordId);
ALTER TABLE `TagWord` MODIFY COLUMN `TagWordId` INT UNSIGNED auto_increment;

-- `Tag` references `Sample`, `TagWord`
CREATE TABLE `Tag` (
    `TagWordId` INT UNSIGNED,
    `SampleId` INT UNSIGNED,
    `TagTime` DATETIME
);
ALTER TABLE `Tag` ADD CONSTRAINT `PK_Tag` PRIMARY KEY (SampleId,TagWordId);
ALTER TABLE `Tag` ADD CONSTRAINT `FK_Tag_SampleId` FOREIGN KEY (SampleId) REFERENCES `Sample`(SampleId);
ALTER TABLE `Tag` ADD CONSTRAINT `FK_Tag_TagWordId` FOREIGN KEY (TagWordId) REFERENCES `TagWord`(TagWordId);


-- ==============================================================================--
-- EVENTS

CREATE TABLE `EventType` (
    `EventTypeId` INT UNSIGNED,
    `EventTypeTitle` VARCHAR(255),
    `EventTypeDescription` TEXT
);
ALTER TABLE `EventType` ADD CONSTRAINT `PK_EventType` PRIMARY KEY (EventTypeId);
ALTER TABLE `EventType` MODIFY COLUMN `EventTypeId` INT UNSIGNED auto_increment;

-- `Event` references `Sample` and `EventType`
CREATE TABLE `Event` (
    `EventId` INT UNSIGNED,
    `EventTypeId` INT UNSIGNED,
    `SampleId` INT UNSIGNED,
    `lft` INT UNSIGNED,
    `rgt` INT UNSIGNED
);
ALTER TABLE `Event` ADD CONSTRAINT `PK_Event` PRIMARY KEY (EventId);
ALTER TABLE `Event` ADD CONSTRAINT `FK_Event_SampleId` FOREIGN KEY (SampleId) REFERENCES `Sample`(SampleId);
ALTER TABLE `Event` ADD CONSTRAINT `FK_Event_EventTypeId` FOREIGN KEY (EventTypeId) REFERENCES `EventType`(EventTypeId);
ALTER TABLE `Event` MODIFY COLUMN `EventId` INT UNSIGNED auto_increment;


-- --------------------------------------------------------------------------------
-- Event Parameters


CREATE TABLE `Parameter` (
    `ParameterId` INT UNSIGNED,
    `ParameterName` VARCHAR(255),
    `ParameterType` CHAR(1), 
      -- B=blob, S=string, N=number, I=integer, M=measurement.
    `ParameterDescription` TEXT
);
ALTER TABLE `Parameter` ADD CONSTRAINT `PK_Parameter` PRIMARY KEY (ParameterId);
ALTER TABLE `Parameter` MODIFY COLUMN `ParameterId` INT UNSIGNED auto_increment;

-- `EventTypeParameters` references `EventTypeId` and `ParameterId`

CREATE TABLE `EventTypeParameters` (
    `EventTypeId` INT UNSIGNED,
    `ParameterId` INT UNSIGNED
);
ALTER TABLE `EventTypeParameters` ADD CONSTRAINT `PK_EventTypeParameters` PRIMARY KEY (EventTypeId,ParameterId);
ALTER TABLE `EventTypeParameters` ADD CONSTRAINT `FK_EventTypeParameters_EventTypeId` FOREIGN KEY (EventTypeId) REFERENCES `EventType`(EventTypeId);
ALTER TABLE `EventTypeParameters` ADD CONSTRAINT `FK_EventTypeParameters_ParameterId` FOREIGN KEY (ParameterId) REFERENCES `Parameter`(ParameterId);


-- parameter values reference `Parameter` and `Event`

CREATE TABLE `ParameterStringValue` (
    `ParameterId` INT UNSIGNED,
    `EventId` INT UNSIGNED,
    `ParameterStringValue` VARCHAR(255)
);
ALTER TABLE `ParameterStringValue` ADD CONSTRAINT `PK_ParameterStringValue` PRIMARY KEY (ParameterId,EventId);
ALTER TABLE `ParameterStringValue` ADD CONSTRAINT `FK_ParameterStringValue_EventId` FOREIGN KEY (EventId) REFERENCES `Event`(EventId);
ALTER TABLE `ParameterStringValue` ADD CONSTRAINT `FK_ParameterStringValue_ParameterId` FOREIGN KEY (ParameterId) REFERENCES `Parameter`(ParameterId);

CREATE TABLE `ParameterNumberValue` (
    `ParameterId` INT UNSIGNED,
    `EventId` INT UNSIGNED,
    `ParameterNumberValue` DOUBLE
);
ALTER TABLE `ParameterNumberValue` ADD CONSTRAINT `PK_ParameterNumberValue` PRIMARY KEY (ParameterId,EventId);
ALTER TABLE `ParameterNumberValue` ADD CONSTRAINT `FK_ParameterNumberValue_ParameterId` FOREIGN KEY (ParameterId) REFERENCES `Parameter`(ParameterId);
ALTER TABLE `ParameterNumberValue` ADD CONSTRAINT `FK_ParameterNumberValue_EventId` FOREIGN KEY (EventId) REFERENCES `Event`(EventId);



-- --------------------------------------------------------------------------------
-- Event Dependencies - references `EventTypeId`

CREATE TABLE `EventDep` (
    `EventDepId` INT UNSIGNED,
    `EventTypeId` INT UNSIGNED, -- one type
    `EventDep` INT UNSIGNED, -- is dependent on other occurring
    `EventDepInvert` BOOLEAN -- or not occurring
);
ALTER TABLE `EventDep` ADD CONSTRAINT `PK_EventDep` PRIMARY KEY (EventDepId);
ALTER TABLE `EventDep` ADD CONSTRAINT `FK_EventDep_EventTypeId` FOREIGN KEY (EventTypeId) REFERENCES `EventType`(EventTypeId);
ALTER TABLE `EventDep` ADD CONSTRAINT `FK_EventDep_EventDep` FOREIGN KEY (EventDep) REFERENCES `EventType`(EventTypeId);
ALTER TABLE `EventDep` MODIFY COLUMN `EventDepId` INT UNSIGNED auto_increment;

-- --------------------------------------------------------------------------------
-- Event Schedule

CREATE TABLE `Schedule` (
    `ScheduleId` INT UNSIGNED,
    `EventId` INT UNSIGNED,
    `ScheduleDatetime` DATETIME
);
ALTER TABLE `Schedule` ADD CONSTRAINT `PK_Schedule` PRIMARY KEY (ScheduleId);
ALTER TABLE `Schedule` ADD CONSTRAINT `FK_Schedule_EventId` FOREIGN KEY (EventId) REFERENCES `Event`(EventId);
ALTER TABLE `Schedule` MODIFY COLUMN `ScheduleId` INT UNSIGNED auto_increment;


-- --------------------------------------------------------------------------------
-- Event Annotations

-- `AnnotationType` references `EventType`
CREATE TABLE `AnnotationType` (
    `AnnotationTypeId` INT UNSIGNED,
    `AnnotationTypeTitle` VARCHAR(255),
    `AnnotationTypeDescription` TEXT,
    `EventTypeId` INT UNSIGNED
);
ALTER TABLE `AnnotationType` ADD CONSTRAINT `PK_AnnotationType` PRIMARY KEY (AnnotationTypeId);
ALTER TABLE `AnnotationType` ADD CONSTRAINT `FK_AnnotationType_EventTypeId` FOREIGN KEY (EventTypeId) REFERENCES `EventType`(EventTypeId);
ALTER TABLE `AnnotationType` MODIFY COLUMN `AnnotationTypeId` INT UNSIGNED auto_increment;

-- `Annotation` references `Event`, `AnnotationType`, `User`
CREATE TABLE `Annotation` (
    `AnnotationId` INT UNSIGNED,
    `AnnotationTypeId` INT UNSIGNED,
    `EventId` INT UNSIGNED,
    `UserId` INT UNSIGNED,
    `AnnotationTime` DATETIME
);
ALTER TABLE `Annotation` ADD CONSTRAINT `PK_Annotation` PRIMARY KEY (AnnotationId);
ALTER TABLE `Annotation` ADD CONSTRAINT `FK_Annotation_EventId` FOREIGN KEY (EventId) REFERENCES `Event`(EventId);
ALTER TABLE `Annotation` ADD CONSTRAINT `FK_Annotation_AnnotationTypeId` FOREIGN KEY (AnnotationTypeId) REFERENCES `AnnotationType`(AnnotationTypeId);
ALTER TABLE `Annotation` ADD CONSTRAINT `FK_Annotation_UserId` FOREIGN KEY (UserId) REFERENCES `User`(UserId);
ALTER TABLE `Annotation` MODIFY COLUMN `AnnotationId` INT UNSIGNED auto_increment;

-- ==============================================================================--
-- EQUIPMENT EVAULATIONS

-- `Equipment` references `EventType`
CREATE TABLE `Equipment` (
    `EquipmentId` INT UNSIGNED,
    `EquipmentName` VARCHAR(255),
    `EquipmentDescription` TEXT,
    `EventTypeId` INT UNSIGNED  -- the type of even this equipment is used for (we're assuming one event-type per equipment here...?)
);
ALTER TABLE `Equipment` ADD CONSTRAINT `PK_Equipment` PRIMARY KEY (EquipmentId);
ALTER TABLE `Equipment` ADD CONSTRAINT `FK_Equipment_EventTypeId` FOREIGN KEY (EventTypeId) REFERENCES `EventType`(EventTypeId);
ALTER TABLE `Equipment` MODIFY COLUMN `EquipmentId` INT UNSIGNED auto_increment;

-- `Evaluation` references `Event`, `Equipment`
CREATE TABLE `Evaluation` (
    `EvaluationId` INT UNSIGNED,
    `EvaluationTitle` VARCHAR(255),
    `EquipmentId` INT UNSIGNED,
    `EventId` INT UNSIGNED
);
ALTER TABLE `Evaluation` ADD CONSTRAINT `PK_Evaluation` PRIMARY KEY (EvaluationId);
ALTER TABLE `Evaluation` ADD CONSTRAINT `FK_Evaluation_EventId` FOREIGN KEY (EventId) REFERENCES `Event`(EventId);
ALTER TABLE `Evaluation` ADD CONSTRAINT `FK_Evaluation_EquipmentId` FOREIGN KEY (EquipmentId) REFERENCES `Equipment`(EquipmentId);
ALTER TABLE `Evaluation` MODIFY COLUMN `EvaluationId` INT UNSIGNED auto_increment;



-- ==============================================================================--
-- AUXILIARY DATA

-- `Table` table, for attaching additional, inconsequential information to the data, like comments and modification logs

CREATE TABLE `Table` (
    `TableId` INT UNSIGNED,
    `TableName` VARCHAR(255)
);
ALTER TABLE `Table` ADD CONSTRAINT `PK_Table` PRIMARY KEY (TableId);
ALTER TABLE `Table` MODIFY COLUMN `TableId` INT UNSIGNED auto_increment;


-- --------------------------------------------------------------------------------
-- comments... these tables reference the `Table` table

CREATE TABLE `TextComment` (
    `TextCommentId` INT UNSIGNED,
    `TableId` INT UNSIGNED,
    `RowId` INT UNSIGNED,
    `Comment` TEXT
);
ALTER TABLE `TextComment` ADD CONSTRAINT `PK_TextComment` PRIMARY KEY (TextCommentId);
ALTER TABLE `TextComment` ADD CONSTRAINT `FK_TextComment_TableId` FOREIGN KEY (TableId) REFERENCES `Table`(TableId);
ALTER TABLE `TextComment` MODIFY COLUMN `TextCommentId` INT UNSIGNED auto_increment;

CREATE TABLE `BlobComment` (
    `BlobCommentId` INT UNSIGNED,
    `TableId` INT UNSIGNED,
    `RowId` INT UNSIGNED,
    `BlobName` VARCHAR(255),
    `BlobMimeType` VARCHAR(255),
    `Comment` MEDIUMBLOB
);
ALTER TABLE `BlobComment` ADD CONSTRAINT `PK_BlobComment` PRIMARY KEY (BlobCommentId);
ALTER TABLE `BlobComment` ADD CONSTRAINT `FK_BlobComment_TableId` FOREIGN KEY (TableId) REFERENCES `Table`(TableId);
ALTER TABLE `BlobComment` MODIFY COLUMN `BlobCommentId` INT UNSIGNED auto_increment;

CREATE TABLE `URLComment` (
    `URLCommentId` INT UNSIGNED,
    `TableId` INT UNSIGNED,
    `RowId` INT UNSIGNED,
    `URLName` VARCHAR(255),
    `URL` TEXT
);
ALTER TABLE `URLComment` ADD CONSTRAINT `PK_URLComment` PRIMARY KEY (URLCommentId);
ALTER TABLE `URLComment` ADD CONSTRAINT `FK_URLComment_TableId` FOREIGN KEY (TableId) REFERENCES `Table`(TableId);
ALTER TABLE `URLComment` MODIFY COLUMN `URLCommentId` INT UNSIGNED auto_increment;

-- --------------------------------------------------------------------------------
-- mod log
CREATE TABLE `Mod` (
    `ModId` BIGINT UNSIGNED,
    `UserId` INT UNSIGNED, -- person who modified
    `TableId` INT UNSIGNED, -- table that was modified
    `ModRowId` INT UNSIGNED, -- row of table that was modified
    `ModDatetime` DATETIME -- modification time
);
ALTER TABLE `Mod` ADD CONSTRAINT `PK_Mod` PRIMARY KEY (ModId);
ALTER TABLE `Mod` ADD CONSTRAINT `FK_Mod_TableId` FOREIGN KEY (TableId) REFERENCES `Table`(TableId);
ALTER TABLE `Mod` ADD CONSTRAINT `FK_Mod_UserId` FOREIGN KEY (UserId) REFERENCES `User`(UserId);
ALTER TABLE `Mod` MODIFY COLUMN `ModId` INT UNSIGNED auto_increment;

