# Usage and required forms

## Users

When a user logs in but does not have a user record
(their UUN is not in the UUN table) they should be
prompted to fill out their user record.

Requires a create-user form.

Users should be able to update their user record
using an update-user form.

### User Form

UserId (hidden/absent)
UserFullName (free text)
UserPreferedName (free text)
UserEmail (validated text)
LabId (select option populated from **Lab**)

### Note

I just observed that User has Lab and LabId... it should
only have LabId.

## Labs and folders

A lab and a folder are necessary for an experiment
to be created.  A user should be able to choose a
lab or not.  But they cannot create a new experiment 
unless they have a lab.

An admin should be able to set up a lab and folder
and also the PI's account for that lab.  Then a user
can select the lab they belong to and start submitting
experiments.

The admin requires forms to add the labs and folders,
although this can be done from phpMyAdmin to start with.

### Lab Form

LabId (hidden/absent)
LabName (free text)
LabLeadUserId (select option populated from **User**)
DefaultExperimentCostTierId (select option populated from 
**ExperimentCostTier**)
LabDefaultFolder (select option populated from **Folder**)

### Folder Form

FolderId (hidden/absent)
Folder (select option generated from 
***detectWritableNetworkFolders***)

As of 14th Jul 2017 (the time of writing) there are no
financial detail fields in the database.  We'll need
a department table, and maybe later a grant table (if
we switch to journalling).  There should be a default
department for each lab, which will be automatically
associated with any expermients newly created by 
members of that lab.  But the user should be able to 
change the departmental and PI details for an experiment.

## Experiment

Once all the preliminaries are sorted, the user can
create experiments.  They'll need an experiment form.

Also, part of this form can be an experiment design
wizard that can generate the samples, associated tags,
categories, events, etc.

### Experiment Form: Simple

ExperimentId (hidden/absent)
ExperimentTypeId (select option populated from 
**ExperimentType**)
ExperimentTitle (free text)
Description (free text)
ExperimentPebbleCode (text disabled?)
ExperimentGroupId (select option populated from 
**ExperimentGroup**)
FolderId (select option populated from **Folder**)
ExperimentCostPounds (text disabled? autocalculated)
ExperimentCostId (select option populated from 
**ExperimentCost** disabled?)
ExperimentSampleCount (validated text: integer)

*Note: "disabled?" means that the item will be displayed*
*but normally disabled for all but admins*

### Experiment Form Wizard

Here's where the magic happens... we can have wizards to 
set up different types of experiment.  More of a
questionnaire than a standard form...



## Samples

A sample table can allow editing of the samples,
their tags and categories.  But this should all be set up 
by the wizard/templates.

### Sample Form

This will more likely be an editable table than a form, 
i.e. editing multiple samples at once.

## Events

The graphical sample/event table should take care of 
annotating events and scheduling.  Having said that,
most of that would be done by an admin or the automated
system anyway.

