#!perl

use strict;
use warnings;
use Data::Dumper;

# work-out-fks.pl

my %tables = ();
my %constraints = ();
my $table = '';
my @fkpk = ();
my %primaries = ();


undef $/;
my $slurp = <DATA>;


while($slurp =~ m/\G.*?(?<=\r\n)([\w].*?)\r\n\r\n(?=\w)/gs){
	my $block = $1;
	if($block =~ /'/){
		my ($tab,$fk,$fkrel,$ftab,$pk,$pkrel);
		if($block =~ /FK\:\s*(\w+)\s+([\d*]|[\d+]\.\.[\d*])/){
			$fk = $1;
			$fkrel = $2;
		}
		if($block =~ /PK\:\s*(\w+)\s+([\d*]|[\d+]\.\.[\d*])/){
			$pk = $1;
			$pkrel = $2;
		}
		if($block =~ /(\w+)'s\s+(\w+)/){
			$tab = $1;
			$ftab = $2;
		}
		if(defined $fk && defined $fkrel && defined $pk && defined $pkrel){
			push @fkpk, [$tab,$fk,$fkrel,$ftab,$pk,$pkrel]
		}
		else {
			die "error detecting parameters in block: $block";
		}
	}
	else {
		my ($table,@block) = split /[\n\r]+\s*/, $block;
		my %fields = map {
			/^\s*([#+\-]?)(\w+)\:\s*(.*?)\s*$/ or die "could not parse block line: $_\n from block: $block";
			if($1 eq '#'){
				if(! exists $primaries{$table}){ $primaries{$table} = {};}
				$primaries{$table}->{$2} = 1;
			}
			($2,$3)
			} @block;
		$tables{$table} = \%fields;
	}
}

# print Dumper \%tables;
# print Dumper \@fkpk;
# print Dumper \%primaries;

# foreach my $fkpk(@fkpk){
# 	my ($tab,$fk,$fkrel,$ftab,$pk,$pkrel) = @$fkpk;
# 	if(exists $primaries{$tab}->{$fk}){
# 		print "$tab $fk  found\n";
# 	}
# 	if(! exists $primaries{$ftab}->{$pk}){
# 		print "$ftab $pk not found\n";
# 	}
# }

foreach my $table(keys %tables){
	print "CREATE TABLE [$table] (\n";
	print join ",\n", map {'    '.$_.' '.$tables{$table}->{$_}} keys %{$tables{$table}};
	print "\n);\n";
	foreach (@fkpk){
		next unless $_->[0] eq $table;
		my ($tab,$fk,$fkrel,$ftab,$pk,$pkrel) = @$_;
		print "ALTER TABLE $table ADD CONSTRAINT FK_${table}_$fk FOREIGN KEY ($fk) REFERENCES $ftab($pk);\n"
	}
	if(exists $primaries{$table}){
		print "ALTER TABLE $table ADD CONSTRAINT PK_$table PRIMARY KEY (".join(',', keys %{$primaries{$table}}).");\n"
	}
	print "\n";
}


exit;

while(<DATA>){
	if(/^([A-Za-z])\s*$/){
		$table = $1;
	}
	elsif(/^\s+(#|\+|-|)([A-Za-z])\:\s*(\w+)/){
		if(! exists $tables{$table}){ $tables{$table} = {}; }
		$tables{$table}->{$2} = $3;
		if($1 eq '#'){
			$constraints{$table.'_pk'} = $2;
		}
	}
}

__DATA__

Lab
    #LabId: number
    +LabName: string
    +LabLeaderUserId: number
    +LabDefaultFolder: string
    +LabDefaultExperimentCostTierId: number

User
    #UserId: number
    +UserFullName: string
    +UserPreferedName: string
    +UserEmail: string
    +LabId: number
    +Lab: Lab

User's Lab
     FK: LabId
    1
     PK: LabId
    1..*

UUN
    #UUN: string (PK)
    +UserId: number

ExperimentGroup
    #ExperimentGroupId: number
    +ExperimentGroupReason: string
    +ExternalId: string
    +ExternalSize: number
    +Date: date
    +Mtr: number = NULL
    +Ftr: number = NULL

Experiment
    #ExperimentId: number
    +ExperimentTitle: string
    +Description: string
    +ExperimentGroup: number = NULL
    +ExperimentPebbleCode: string
    +ExperimentTypeId: number
    +ExperimentSampleCount: number
    +ExperimentCostId: number
    +ExperimentCostPounds: number
    +FolderId: number

UUN's User
     PK: UserId
    1
     FK: UserId
    1..*

Experiment's ExperimentGroup
     PK: ExperimentGroupId
    1
     FK: ExperimentGroupId
    1..*

ExperimentOwners
    #ExperimentId: number
    #UserId: number

ExperimentOwners's User
     PK: UserId
    1..*
     FK: UserId
    0..*

ExperimentOwners's Experiment
     PK: ExperimentId
    1..*
     FK: ExperimentId
    1..*

ExperimentCost
    #ExperimentCostId: number
    +ExperimentCostTierId: number
    +ExperimentTypeId: number
    +ExperimentCostPerSample: number

ExperimentCostTier
    #ExperimentCostTierId: number
    +ExperimentCostTierTitle: string
    +ExperimentCostTierDescription: string

ExperimentType
    #ExperimentTypeId: number
    +ExperimentTypeTitle: string
    +ExperimentTypeDescription: string

Sample
    #SampleId: number
    +ExperimentId: number
    +SampleName: string
    +SampleComment: string
    -lft: number
    -rgt: number

Event
    #EventId: number
    +EventTypeId: number
    +SampleId: number
    -lft: number
    -rgt: number

EventType
    #EventTypeId: number
    +EventTypeTitle: string
    +EventTypeDescription: string
    +ParameterSetId: number

Annotation
    #AnnotationId: number
    +AnnotationTypeId: number
    +EventId: number
    +UserId: number
    +AnnotationTime: DateTime

AnnotationType
    #AnnotationTypeId: number
    +AnnotationTypeTitle: string
    +AnnotationTypeDescription: string
    +EventTypeId: number

Schedule
    #ScheduleId: number
    +EventId: number
    +ScheduleDateTime: datetime

EventTypeParameters
    #EventTypeId: number
    #ParameterId: number

Parameter
    #ParameterId: number
    +ParameterName: string
    +ParameterDescription: string
    +ParameterType: ENUM(number,string) = number

ParameterNumberValue
    #ParameterId: number
    #EventId: number
    +ParameterNumberValue: number

ParameterStringValue
    #ParameterId: number
    #EventId: number
    +ParameterStringValue: string

Experiment's ExperimentCost
     PK: ExperimentCostId
    1
     FK: ExperimentCostId
    0..*

ExperimentCost's ExperimentType
     PK: ExperimentTypeId
    1
     FK: ExperimentTypeId
    0..*

ExperimentCosts's ExperimentCostTier
     PK: ExperimentCostTierId
    1
     FK: ExperimentCostTierId
    0..*

Sample's Experiment
     PK: ExperimentId
    1
     FK: ExperimentId
    1..*

Event's Sample
     PK: SampleId
    1
     FK: SampleId
    1..*

Annotation's Event
     PK: EventId
    1
     FK: EventId
    0..*

Event's EventType
     PK: EventTypeId
    1
     FK: EventTypeId
    1..*

Annotation's AnnotationType
     PK: AnnotationTypeId
    1
     FK: AnnotationTypeId
    0..*

EventTypeParameters's EventType
     PK: EventTypeId
    1
     FK: EventTypeId
    0..*

ParameterStringValue's Event
     PK: EventId
    1
     FK: EventId
    0..*

ParameterNumberValue's Parameter
     PK: ParameterId
    1
     FK: ParameterId
    0..*

ParameterStringValue's Parameter
     PK: ParameterId
    1
     FK: ParameterId
    0..*

Schedule's Event
     PK: EventId
    1
     FK: EventId
    1

Annotation's User
     PK: UserId
    1
     FK: UserId
    1

Experiment's ExperimentType
     PK: ExperimentTypeId
    1
     FK: ExperimentTypeId
    0..*

Lab's ExperimentCostTier
     PK: ExperimentCostTierId
    1
     FK: ExperimentCostTierId
    0..*

Lab's User
     FK: LabLeaderUserId
    1
     PK: UserId
    1

UserPrivileges
    #UserId: number
    #PrivilegeId: number

Privilege
    #PrivilegeId: number
    +PrivilegeTitle: string
    +PrivilegeDescription: string

UserPrivileges's User
     PK: UserId
    1
     FK: UserId
    0..*

UserPrivileges's Privilege
     PK: PrivilegeId
    1
     FK: PrivilegeId
    0..*

Folder
    #FolderId: number
    +Folder: string

Experiment's Folder
     PK: FolderId
    1
     FK: FolderId
    1..*

EventTypeParameters's Parameter
     PK: ParameterId
    1
     FK: ParameterId
    0..*

StatusType
    #StatusTypeId: number
    +StatusTypeTitle: string
    +StatusTypeDescription: string

Status
    #StatusId: number
    +StatusTypeId: number
    +ExperimentId: number
    +StatusTime: DateTime
    +UserId: number

Status's Experiment
     FK: ExperimentId
    0..*
     PK: ExperimentId
    1

Status's StatusType
     FK: StatusTypeId
    0..*
     PK: StatusTypeId
    1

TagWord
    #TagWordId: number
    +TagWord: string
    +TagWordDescription: string
    -lft: number
    -rgt: number

Tag
    #TagId: number
    +TagWordId: number
    +SampleId: number
    +TagTime: DateTime

Tag's Sample
     FK: SampleId
    0..*
     PK: SampleId
    1

Tag's TagWord
     FK: TagWordId
    0..*
     PK: TagWordId
    1

AnnotationType's EventType
     PK: EventTypeId
    1
     FK: EventTypeId
    0..*

SampleCategory
    #SampleCategoryId: number
    +SampleId: number
    +CategoryTime: DateTime
    +CategoryId: number

CategorySet
    #CategorySetId: number
    +CategorySetName: string
    +CategorySetDescription: string

SampleCategory's CategorySet
     FK: CategorySetId
    0..*
     PK: CategorySetId
    1

SampleCategory's Sample
     FK: SampleId
    0..*
     PK: SampleId
    1

Table
    #TableId: number
    +TableName: string

URLComment
	#URLCommentId: number
    +TableId: number
    +RowId: number
    +URL: Text
    +URLName: string

URLComment's Table
     PK: TableId
    1
     FK: TableId
    0..*

TextComment
	#TextCommentId: number
    +TableId: number
    +RowId: number
    +Comment: Text

TextComment's Table
     PK: TableId
    1
     FK: TableId
    0..*

BlobComment
	#BlobCommentId: number
    +TableId: number
    +RowId: number
    +Blob: Blob
    +BlobName: string
    +BlobMimeType: string

BlobComment's Table
     PK: TableId
    1
     FK: TableId
    0..*

EventDep
    #EventDepId: number
    +EventTypeId: number
    +EventDep: number
    +EventDepInvert: bool

EventDep's EventType
     PK: EventTypeId
    1
     FK: EventTypeId
    1..*

EventDep's EventType
     PK: EventTypeId
    1
     FK: EventDep
    1..*

ParameterNumberValue's Event
     PK: EventId
    1
     FK: EventId
    0..*

Equipment
    #EquipmentId: number
    +EventTypeId: number
    +EquipmentName: string

Evaluation
    #EvaluationId: number
    +EventId: number
    +EquipmentId: number
    +EvaluationTitle: string

Evaluation's Event
     PK: EventId
    1
     FK: EventId
    1

Evaluation's Equipment
     FK: EquipmentId
    0..*
     PK: EquipmentId
    1

Equipment's EventType
     FK: EventTypeId
    0..*
     PK: EventTypeId
    1

Category
    #CategoryId: number
    +CategorySetId: number
    +CategoryTitle: text
    +CategoryTime: DateTime

SampleCategory's Category
     FK: CategoryId
    0..*
     PK: CategoryId
    1

Category's CategorySet
     FK: CategorySetId
    0..*
     PK: CategorySetId
    1
