# django

so I think I've pretty much decided to use the django framework.  It's really
nice and solves a lot of the issues I've raised.

One outstanding issue is that of nested sets...

I'm just trying to think if any of the uses of hierarchy in my system 
are actually required anywhere except the UI...

Samples are organised graphically by the nested sets, but otherwise the
parent/level are the only important infos.

Organisation of other hierarchies is mostly for ease of viewing... right?

If it's all for UI, then...

1) we only need to query the whole collection, and use the sets in the UI
2) we might not need nested sets at all...

Have a think...
