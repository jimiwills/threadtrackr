SET NAMES 'utf8'; -- add this to all connections to ensure utf8 names are okay
CREATE USER IF NOT EXISTS mousedb@localhost;
SET PASSWORD FOR mousedb@localhost = PASSWORD('xRof3ZNAN5hAnlJ4');
GRANT ALL ON mousedb.* TO mousedb;

DROP DATABASE mousedb; ---- DELETE THIS WHEN YOU WANT TO START STORING DATA!

CREATE DATABASE IF NOT EXISTS mousedb DEFAULT CHARACTER SET = 'utf8';
USE mousedb;

CREATE TABLE IF NOT EXISTS Tissue (
    id SMALLINT UNSIGNED PRIMARY KEY AUTO_INCREMENT, -- range is 0 to 65535
    title VARCHAR(255),  -- 255 bytes
    INDEX (title),
    UNIQUE KEY (title)
);

CREATE TABLE IF NOT EXISTS Structure (
    id SMALLINT UNSIGNED PRIMARY KEY AUTO_INCREMENT, -- range is 0 to 65535
    title VARCHAR(255),  -- 255 bytes
    INDEX (title),
    UNIQUE KEY (title)
);

CREATE TABLE IF NOT EXISTS relTissueStructure (
    fkTissue SMALLINT UNSIGNED NOT NULL,                         -- the first
    fkStructure SMALLINT UNSIGNED NOT NULL,                     -- the second - almost identical
    
    PRIMARY KEY (fkTissue,fkStructure),   -- primary key is the pair of foreign keys

    FOREIGN KEY (fkTissue)
      REFERENCES Tissue(id)
      ON UPDATE CASCADE ON DELETE RESTRICT,
    FOREIGN KEY (fkStructure)                     -- set up fk...
      REFERENCES Structure(id)
      ON UPDATE CASCADE ON DELETE RESTRICT          -- maintaoin data integrity

      
);

CREATE TABLE IF NOT EXISTS Table1 (
    id INT NOT NULL AUTO_INCREMENT, -- regular id for this table
    cfkTissueStructure_Tissue SMALLINT UNSIGNED NOT NULL,  -- these two will point to a combined foreign key...
    cfkTissueStructure_Structure SMALLINT UNSIGNED NOT NULL, -- which are in turn fks for other tables!

    PRIMARY KEY(id),
    INDEX (cfkTissueStructure_Tissue, cfkTissueStructure_Structure), -- this combo has to be present in relTissueStructure

    FOREIGN KEY (cfkTissueStructure_Tissue, cfkTissueStructure_Structure) -- set up fk...
      REFERENCES relTissueStructure(fkTissue, fkStructure)
      ON UPDATE CASCADE ON DELETE RESTRICT                              -- maintain data integrity

);

-- do some populating...

INSERT INTO Tissue VALUES (1,'Muscle') ;
INSERT INTO Tissue VALUES (2,'Bone');

INSERT INTO Structure VALUES (1,'Limb');
INSERT INTO Structure VALUES (2,'Heart');

INSERT INTO relTissueStructure VALUES (1,1);
INSERT INTO relTissueStructure VALUES (1,2);
INSERT INTO relTissueStructure VALUES (2,1); -- but not 2,2 because there's no bone in heart (or there shouldn't be)

INSERT INTO Table1 VALUES (1,1,1);



SELECT Tissue.id, Tissue.Title, Structure.id, Structure.Title FROM relTissueStructure 
    JOIN Tissue ON relTissueStructure.fkTissue = Tissue.id 
    JOIN Structure ON relTissueStructure.fkStructure = Structure.id;

-- OUTPUTS:
-- id      Title   id      Title
-- 2       Bone    1       Limb
-- 1       Muscle  1       Limb
-- 1       Muscle  2       Heart


SELECT Table1.id, Tissue.Title, Structure.Title FROM Table1 
    JOIN Tissue ON Table1.cfkTissueStructure_Tissue = Tissue.id 
    JOIN Structure ON Table1.cfkTissueStructure_Structure = Structure.id;

-- OUTPUTS:
-- id      Title   Title
-- 1       Muscle  Limb


INSERT INTO Table1 VALUES (2,2,2);

-- OUTPUTS:
-- ERROR 1452 (23000) at line 82: Cannot add or update a child row: 
--   a foreign key constraint fails (`mousedb`.`table1`, CONSTRAINT 
--   `table1_ibfk_1` FOREIGN KEY (`cfkTissueStructure_Tissue`, 
--   `cfkTissueStructure_Structure`) REFERENCES `reltissuestructure` 
--   (`fkTissue`, `fkStructure`) ON UPDATE C)

-- ... because 2,2 would be (Bone,Heart) - which does not exist in relTissueStructure
