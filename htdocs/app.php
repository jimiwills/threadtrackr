<?php

$model = 'Table1.list'; # default model
$view = 'Table1.table'; # default view // can we make views generic for models?
// no alternative controllers yet... that'll come later

require_once("inc/connect.inc.php");
$dbo = dbconnect();
require_once("models/$model.inc.php");
require_once("views/$view.inc.php");

$model_params = array();
$view_params = array(); // these'll need to come from the environment somewhere

$result = view(model($dbo, $model_params), $view_params); // should separate these to allow error handling

print $result;
